package campaign.content.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.activation.MimeType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import campaign.content.domain.FileInfo;
import campaign.content.util.Constants;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

@Component
public class StreamingMongoDaoImpl implements StreamingDao {
	@Autowired
	GridFsTemplate gridFsTemplate;
	private DBObject dbobject;

	//	@Value("${serverUrl}")
	//	private String serverUrl;
	//
	//	@Value("${streamVideoMapping}")
	//	private String streamVideoMapping;
	//
	//	@Value("${streamImageMapping}")
	//	private String streamImageMapping;

	public GridFsTemplate getGridFsTemplate() {
		return gridFsTemplate;
	}

	public void setGridFsTemplate(GridFsTemplate gridFsTemplate) {
		this.gridFsTemplate = gridFsTemplate;
	}

	@Override
	public InputStream getInputStreamById(String tenantId, FileInfo fileInfo) {
		GridFSDBFile file = getGridFsTemplate().findOne(
				new Query(Criteria.where("_id").is(fileInfo.getFileId())));

		if(file != null){
			fileInfo.setFileName(file.getFilename());
			fileInfo.setLength(file.getLength());
			fileInfo.setExtension((String) file.getMetaData().get("extension"));
			fileInfo.setContentType(file.getContentType());
			fileInfo.setTenantId((String) file.getMetaData().get("tenantId"));
		}
		return file.getInputStream();
	}

	@Override
	public FileInfo saveFile(String tenantId, Map<String, Object> fieldMap)
			throws IOException {

		FileInfo fileInfo = new FileInfo();
		GridFSFile gridFile;
		String contentType = (String) fieldMap.get("contentType");
		dbobject = new BasicDBObject();
		dbobject.put("tenantId", tenantId);
		// if its video then we have to save its thumbnail
		if (contentType.contains("video")) {
			try {

				// For Thumb Id
				File thumbFile = new File((String) fieldMap.get("thumb"));
				FileInputStream thumbinputStrm = new FileInputStream(thumbFile);
				new MimeType().getBaseType();

				DBObject dbobj = new BasicDBObject();
				dbobj.put("extension", "jpg");
				dbobj.put("tenantId", tenantId);
				gridFile = getGridFsTemplate().store(thumbinputStrm,
						thumbFile.getName(),
						Constants.ContentType.IMAGETYPE.stringValue(), dbobj);
				thumbinputStrm.close();
				dbobject.put("thumbId", gridFile.getId()); // thumbId
				dbobject.put("durationInSecs", fieldMap.get("durationInSecs"));
				dbobject.put("duration", fieldMap.get("duration"));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		dbobject.put("extension", fieldMap.get("extension"));
		if (fieldMap.get("file") instanceof MultipartFile) {
			MultipartFile file = (MultipartFile) fieldMap.get("file");
			InputStream is = file.getInputStream();
			gridFile = getGridFsTemplate().store(is,
					String.valueOf(fieldMap.get("fileName")), contentType,
					dbobject);
			is.close();
//			fileInfo = new FileInfo(gridFile.getId().toString(),
//					gridFile.getFilename(), gridFile.getLength(),
//					(String) gridFile.getMetaData().get("extension"),
//					gridFile.getContentType(), (String) gridFile.getMetaData()
//					.get("tenantId"));
			
			String thumbId = "";
			String durationInSecs = "";
			if(gridFile.getMetaData() != null && gridFile.getMetaData().get("thumbId") != null){
				thumbId = gridFile.getMetaData().get("thumbId").toString();
			}
			if(gridFile.getMetaData() != null && gridFile.getMetaData().get("durationInSecs") != null){
				durationInSecs = gridFile.getMetaData().get("durationInSecs").toString();
			}
			
			fileInfo = new FileInfo(gridFile.getId().toString(),
					gridFile.getFilename(), gridFile.getLength(),
					(String) gridFile.getMetaData().get("extension"),
					gridFile.getContentType(), durationInSecs, thumbId, 
					(String) gridFile.getMetaData().get("tenantId"));

		} else {
			try {
				File file = (File) fieldMap.get("file");
				FileInputStream is = new FileInputStream(file);
				new MimeType().getBaseType();
				String cType = contentType.contains("video") ? Constants.ContentType.VIDEO_MP4
						.stringValue()
						: contentType.contains("image") ? Constants.ContentType.IMAGETYPE
								.stringValue() : "";
								gridFile = getGridFsTemplate().store(is,
										String.valueOf(fieldMap.get("fileName")), cType,
										dbobject);
								is.close();
								fileInfo = new FileInfo(gridFile.getId().toString(),
										gridFile.getFilename(), gridFile.getLength(),
										(String) gridFile.getMetaData().get("extension"),
										gridFile.getContentType(), (String) gridFile
										.getMetaData().get("tenantId"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return fileInfo;
	}

	@Override
	public List<FileInfo> findAllFiles(String tenantId) {
		List<GridFSDBFile> results = getGridFsTemplate().find(
				new Query(Criteria.where("tenantId").is(tenantId)));

		List<FileInfo> fileInfoList = new ArrayList<FileInfo>();
		FileInfo fileInfo = null;

		for (GridFSDBFile gridFSDBFile : results) {
			fileInfo = new FileInfo(gridFSDBFile.getId().toString(),
					gridFSDBFile.getFilename(), gridFSDBFile.getLength(),
					(String) gridFSDBFile.getMetaData().get("extension"),
					gridFSDBFile.getContentType(), (String) gridFSDBFile
					.getMetaData().get("tenantId"));

			fileInfoList.add(fileInfo);
		}

		return fileInfoList;
	}

	public String deleteFile(String tenantId, String fileId) {
		GridFSDBFile filemetaData = gridFsTemplate.findOne(new Query(Criteria
				.where("_id").is(fileId).and("tenantId").is(tenantId)));
		if (filemetaData == null) {
			return "File with ID: " + fileId + " doesn't exist!!!";
		}
		DBObject dbObject = filemetaData.getMetaData();
		getGridFsTemplate().delete(
				new Query(Criteria.where("tenantId")
						.is(dbObject.get("tenantId")).and("_id").is(fileId)));
		if (dbObject.get("thumbId") != null)
			getGridFsTemplate().delete(
					new Query(Criteria.where("tenantId")
							.is(dbObject.get("tenantId")).and("_id")
							.is(dbObject.get("thumbId"))));

		return "File with ID: " + fileId + " deleted successfully!!!";
	}

	@Override
	public FileInfo findFileById(String tenantId, String id) {
		GridFSDBFile gridFSDBFile = getGridFsTemplate().findOne(
				new Query(Criteria.where("_id").is(id).and("tenantId")
						.is(tenantId)));
		if (gridFSDBFile != null) {
			FileInfo fileInfo = new FileInfo(gridFSDBFile.getId().toString(),
					gridFSDBFile.getFilename(), gridFSDBFile.getLength(),
					(String) gridFSDBFile.getMetaData().get("extension"),
					gridFSDBFile.getContentType(), (String) gridFSDBFile
					.getMetaData().get("tenantId"));

			return fileInfo;
		}
		return new FileInfo();
	}
}
