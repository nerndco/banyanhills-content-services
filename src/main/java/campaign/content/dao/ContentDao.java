package campaign.content.dao;

import java.util.List;

import campaign.content.domain.Content;

import com.mysema.query.types.Predicate;


public interface ContentDao {
	public List<Content> findAllContent(String tenantId);

	public Content findContentById(String tenantId, String id);

	public List<Content> findContentByName(String tenantId, String name);

	public Content createContent(Content content);

	public Content updateContentById(Content content);

	public String deleteContentById(String tenantId, String id);

	public List<Content> searchByContentCriteria(Predicate predicate);
}
