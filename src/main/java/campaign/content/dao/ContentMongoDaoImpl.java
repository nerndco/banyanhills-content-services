package campaign.content.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import campaign.content.domain.Content;
import campaign.content.repositories.ContentRepository;

import com.mysema.query.types.Predicate;


@Component
public class ContentMongoDaoImpl implements ContentDao {

	@Autowired
	private ContentRepository contentRepository;
	/**
	 * @return the contentRepository
	 */
	public ContentRepository getContentRepository() {
		return contentRepository;
	}
	/**
	 * @param contentRepository the contentRepository to set
	 */
	public void setContentRepository(ContentRepository contentRepository) {
		this.contentRepository = contentRepository;
	}
	@Override
	public Content findContentById(String tenantId, String id) {
		return getContentRepository().findContentByTenantIdAndId(tenantId, id);
	}
	@Override
	public List<Content> findContentByName(String tenantId, String name) {
		return getContentRepository().findContentByTenantIdAndName(tenantId, name);
	}
	@Override
	public Content createContent(Content content) {
		return getContentRepository().save(content);
	}
	@Override
	public Content updateContentById(Content content) {
		return getContentRepository().save(content);
	}
	@Override
	public String deleteContentById(String tenantId, String id) {
		getContentRepository().delete(findContentById(tenantId, id));
		return "Content with ID: "+id+" deleted successfully!!!";
	}
	@Override
	public List<Content> findAllContent(String tenantId) {
		return getContentRepository().findContentByTenantId(tenantId);
	}

	@Override
	public List<Content> searchByContentCriteria(Predicate predicate) {
		return (List<Content>) getContentRepository().findAll(predicate);
	}

}
