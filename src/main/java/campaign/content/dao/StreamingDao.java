package campaign.content.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import campaign.content.domain.FileInfo;

public interface StreamingDao {

	public InputStream getInputStreamById(String tenantId, FileInfo fileInfo);

	public List<FileInfo> findAllFiles(String tenantId);

	public String deleteFile(String tenantId, String fileId);

	public FileInfo saveFile(String tenantId, Map<String, Object> fieldMap) throws IOException;

	public FileInfo findFileById(String tenantId, String id);
}
