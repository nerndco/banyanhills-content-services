package campaign.content.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import campaign.content.dao.ContentDao;
import campaign.content.domain.Content;
import campaign.content.domain.FileInfo;
import campaign.content.service.StreamingService;

import com.mysema.query.types.Predicate;

@RestController
@RequestMapping("/content")
public class ContentController {
	static final Logger LOG = LoggerFactory.getLogger(ContentController.class);
	@Autowired
	private ContentDao contentDao;
	@Autowired
	private StreamingService streamingService;

	/**
	 * @return the contentDao
	 */
	public ContentDao getContentDao() {
		return contentDao;
	}

	/**
	 * @return the streamingService
	 */
	public StreamingService getStreamingService() {
		return streamingService;
	}

	/**
	 * Welcome Method
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOG.info("welcome() method called");
		return "Welcome to the Content Service";
	}

	// Content Related Services

	/**
	 * Method for handling create content request from client
	 */
	@RequestMapping(value = "/api/createContent", consumes = "application/json", method = RequestMethod.POST)
	@ResponseBody
	public Content createContent(@RequestBody Content content) {
		LOG.info("createContent(content) method called");
		return getContentDao().createContent(content);
	}

	/**
	 * Method for handling list all content request from client
	 */
	@RequestMapping(value = "/api/listAllContent", method = RequestMethod.GET)
	@ResponseBody
	public List<Content> findAllContent(
			@RequestParam("tenantId") String tenantId) {
		LOG.info("findAllContent(tenantId) method called");
		return getContentDao().findAllContent(tenantId);
	}

	/**
	 * Method for handling list content by id request from client
	 */
	@RequestMapping(value = "/api/listContentById", method = RequestMethod.GET)
	@ResponseBody
	public Content findContentById(@RequestParam("tenantId") String tenantId,
			@RequestParam("id") String id) {
		LOG.info("findContentById(tenantId,id) method called");
		return getContentDao().findContentById(tenantId, id);
	}

	/**
	 * Method for handling list content by name request from client
	 */
	@RequestMapping(value = "/api/listContentByName", method = RequestMethod.GET)
	@ResponseBody
	public List<Content> findContentByName(
			@RequestParam("tenantId") String tenantId,
			@RequestParam("name") String name) {
		LOG.info("findContentByName(tenantId,name) method called");
		return getContentDao().findContentByName(tenantId, name);
	}

	/**
	 * Method for handling update content request from client
	 */
	@RequestMapping(value = "/api/updateContent", consumes = "application/json", method = RequestMethod.PUT)
	@ResponseBody
	public Content updateContentById(@RequestBody Content content) {
		LOG.info("updateContentById(content) method called");
		LOG.info("Update  contentId: " + content.getId());
		return getContentDao().updateContentById(content);
	}

	/**
	 * Method for handling delete content request from client
	 */
	@RequestMapping(value = "/api/deleteContent", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteContentById(@RequestParam("tenantId") String tenantId,
			@RequestParam("id") String id) {
		LOG.info("deleteContentById(tenantId,id) method called");
		LOG.info("Delete  contentId: " + id);
		try {
			getContentDao().deleteContentById(tenantId, id);
			return "Content with ID: " + id + " Deleted Successfully";
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	/**
	 * Method for handling search content request from client
	 */

	@RequestMapping(value = "/api/searchContent", method = RequestMethod.GET)
	@ResponseBody
	public List<Content> searchByContentCriteria(
			@QuerydslPredicate(root = Content.class) Predicate predicate,
			@RequestParam MultiValueMap<String, String> parameters) {
		LOG.info("searchByContentCriteria() method called");
		return (List<Content>) getContentDao().searchByContentCriteria(
				predicate);
	}

	// Files Related Services

	/**
	 * Method for handling upload files request from client
	 */
	@RequestMapping(value = "/api/uploadFiles", method = RequestMethod.POST)
	@ResponseBody
	public List<FileInfo> handleFileUpload(HttpServletRequest request,
			@RequestParam("file") MultipartFile[] file) {
		String tenantId = request.getHeader("tenantId");
		LOG.info("handleFileUpload(tenantId,file) method called:" + file);
		try {
			List<FileInfo> listFileInfo = getStreamingService().doUpload(
					tenantId, file);
			return listFileInfo;
		} catch (IOException e) {
			e.printStackTrace();
			Assert.notNull(e, e.getMessage());
			LOG.error("Error in uploadFiles", e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Assert.notNull(e, e.getMessage());
			LOG.error("Error in uploadFiles", e);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.notNull(e, e.getMessage());
			LOG.error("Error in uploadFiles", e);
		}
		return null;
	}
	
	
	@RequestMapping(value = "/api/uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public List<FileInfo> handleFileUpload(HttpServletRequest request,
			@RequestParam("file") MultipartFile file) {
		String tenantId = request.getHeader("tenantId");
		LOG.info("handleFileUpload(tenantId,file) method called");
		try {
			List<FileInfo> listFileInfo = getStreamingService().doUpload(
					tenantId, file);
			return listFileInfo;
		} catch (IOException e) {
			e.printStackTrace();
			Assert.notNull(e, e.getMessage());
			LOG.error("Error in uploadFiles", e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Assert.notNull(e, e.getMessage());
			LOG.error("Error in uploadFiles", e);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.notNull(e, e.getMessage());
			LOG.error("Error in uploadFiles", e);
		}
		return null;
	}

	/**
	 * Method for handling digital signature of files request from client
	 */
	@RequestMapping(value = "/api/digitalSignature", method = RequestMethod.POST)
	@ResponseBody
	public String handleDigitalSignature(HttpServletRequest request,
			@RequestParam("file") MultipartFile[] file) {
		String tenantId = request.getHeader("tenantId");
		LOG.info("handleDigitalSignature(tenantId,file) method called");
		try {
			FileInfo fileInfo = getStreamingService().doDigitalSignature(
					tenantId, file);
			return fileInfo.toString();
		} catch (IOException e) {
			return e.getMessage();
		} catch (InterruptedException e) {
			return e.getMessage();
		}

	}

	/**
	 * Method for handling list of files request from client
	 */
	@RequestMapping(value = "/api/listAllFiles", method = RequestMethod.GET)
	@ResponseBody
	public List<FileInfo> findAllFiles(String tenantId) {
		LOG.info("findAllFiles(tenantId) method called");
		return getStreamingService().fileList(tenantId);
	}

	/**
	 * Method for handling list file by id request from client
	 */
	@RequestMapping(value = "/api/listFileById", method = RequestMethod.GET)
	@ResponseBody
	public FileInfo findFileById(HttpServletRequest request,
			@RequestParam("fileId") String fileId) {
		String tenantId = request.getHeader("tenantId");
		LOG.info("findFileById(tenantId, fileId) method called");
		return getStreamingService().findFileById(tenantId, fileId);
	}

	/**
	 * Method for handling streaming of file request from client
	 */
	@RequestMapping(value = "/api/streamFile", method = RequestMethod.GET)
	@ResponseBody
	public void streamFile(HttpServletRequest request,
			HttpServletResponse response) {
		LOG.info("streamFile(request,response) method called");
		try {
			getStreamingService().doStreaming(request, response);
		} catch (Exception e) {
			response.setStatus(500, e.getMessage());
		}
	}

	/**
	 * Method for handling file delete request from client
	 */
	@RequestMapping(value = "/api/deleteFile", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteFileById(HttpServletRequest request,
			@RequestParam("fileId") String fileId) {

		String tenantId = request.getHeader("tenantId");
		LOG.info("deleteFileById(fileId) method called");
		LOG.info("Delete  fileId: " + fileId);
		try {
			getStreamingService().deleteFile(tenantId, fileId);
			return "File with FileID: " + fileId + " Deleted Successfully";
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	/**
	 * Method for handling file download request from client
	 */
	@RequestMapping(value = "/api/downloadFile", method = RequestMethod.GET)
	public void doDownload(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		LOG.info("doDownload(request,response) method called");
		try {
			getStreamingService().doFileDownload(request, response);
		} catch (Exception e) {
			response.setStatus(500, e.getMessage());
		}
	}
}
