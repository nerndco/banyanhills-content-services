package campaign.content.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import campaign.content.dao.StreamingDao;
import campaign.content.domain.FileInfo;
import campaign.content.util.Constants;
import campaign.content.util.FLVConverter;
import campaign.content.util.Range;
import campaign.content.util.RangeParser;

@Component
public class StreamingServiceImpl implements StreamingService {
	static final Logger LOG = LoggerFactory
			.getLogger(StreamingServiceImpl.class);

	@Autowired
	private StreamingDao streamingDao;

	@Value("${ffmpegexePath}")
	private String ffmpegexePath;
	@Value("${qtfaststartPath}")
	private String qtfaststartPath;
	@Value("${ffprobeexePath}")
	private String ffprobeexePath;
	@Value("${thumbHour}")
	private String thumbHour;
	@Value("${thumbMinute}")
	private String thumbMinute;
	@Value("${thumbSecond}")
	private String thumbSecond;
	@Value("${thumbWidth}")
	private String thumbWidth;
	@Value("${thumbHeight}")
	private String thumbHeight;

	public StreamingDao getStreamingDAO() {
		return streamingDao;
	}

	public void setStreamingDAO(StreamingDao streamingDao) {
		this.streamingDao = streamingDao;
	}

	@Override
	public List<FileInfo> fileList(String tenantId) {
		return getStreamingDAO().findAllFiles(tenantId);
	}

	@Override
	public void doStreaming(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BufferedInputStream bis = null;
		List<Range> ranges = null;
		String tenantId = request.getHeader("tenantId");
		FileInfo fileInfo = new FileInfo(request.getParameter("fileId"));

		RangeParser rangeParser = new RangeParser();

		response.setHeader("Cache-Control", "no-store, must-revalidate");
		response.setHeader("Expires", "Sat, 26 Jul 1997 05:00:00 GMT");
		response.setHeader("Accept-Ranges", "bytes");

		InputStream is = getStreamingDAO().getInputStreamById(tenantId,
				fileInfo);
		// gets MIME type of the file
		String mimeType = fileInfo.getContentType();
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		// set content attributes for the response
		response.setContentType(mimeType);

		ranges = rangeParser.parseRange(request, response, fileInfo);
		ServletOutputStream oStream = response.getOutputStream();
		long tmpLength = fileInfo.getLength();
		if (ranges == null) {
			response.setHeader("Content-Length", "" + String.valueOf(tmpLength));
			bis = new BufferedInputStream(is);
			byte arr[] = new byte[1024];
			while ((tmpLength = bis.read(arr)) > 0) {
				oStream.write(arr);
			}
			is.close();
		} else {
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
			response.setBufferSize(Constants.Size.responseBufferSize.value);
			if (ranges.size() == 1) {
				Range range = ranges.get(0);
				response.addHeader("Content-Range", "bytes " + range.start
						+ "-" + range.end + "/" + range.length);
				tmpLength = range.end - range.start + 1;
				response.setHeader("content-length", String.valueOf(tmpLength));
				copy(is, oStream, range);
			} else {
				response.setContentType(Constants.ContentType.MULTIPART_BYTERANGE
						.stringValue());
				copy(is, oStream, ranges.iterator(),
						Constants.ContentType.MULTIPART_BYTERANGE.stringValue());
			}
		}

	}

	/**
	 * Copy ranges of content of the specified input stream to the specified
	 * output stream, and ensure that the input stream is closed before
	 * returning (even in the face of an exception).
	 * 
	 * @param istream
	 *            InputStream to read data from
	 * @param ostream
	 *            ServletOutputStream to write to
	 * @param ranges
	 *            Enumeration of the ranges the client wanted to retrieve
	 * @param contentType
	 *            Content type of the resource
	 * @throws IOException
	 *             if an input/output error occurs
	 */
	public void copy(InputStream istream, ServletOutputStream ostream,
			Iterator<Range> ranges, String contentType) throws Exception {

		IOException exception = null;
		while (exception == null && ranges.hasNext()) {
			Range currentRange = ranges.next();
			// Writing MIME header.
			ostream.println();
			ostream.println("--" + Constants.MimeType.mimeSeparation + "--");
			ostream.println("Content-Type: " + contentType);

			ostream.println("Content-Range: bytes" + currentRange.start + "-"
					+ currentRange.end + "/" + currentRange.length);
			// Printing content
			exception = copyRange(istream, ostream, currentRange.start,
					currentRange.end);
			istream.close();
		}
		ostream.println();
		ostream.print("--" + Constants.MimeType.mimeSeparation + "--");
		if (exception != null) {
			throw new Exception();
		}
	}

	public void copy(InputStream istream, ServletOutputStream ostream,
			Range range) throws Exception {
		Exception exception = copyRange(istream, ostream, range.start,
				range.end);
		istream.close();
		if (exception != null) {
			throw exception;
		}
	}

	/**
	 * Copy a range of contents of the specified input stream to the specified
	 * output stream.
	 * 
	 * @param istream
	 *            The input stream to read from
	 * @param ostream
	 *            The output stream to write to
	 * @param start
	 *            Start of the range which will be copied
	 * @param end
	 *            End of the range which will be copied
	 * @return Exception which occurred during processing or null if none
	 *         encountered
	 */
	public IOException copyRange(InputStream istream,
			ServletOutputStream ostream, long start, long end)
			throws IOException {
		long skipped = 0;
		skipped = istream.skip(start);

		if (skipped < start) {
			return new IOException("start is less than skipped");
		}

		IOException exception = null;
		long bytesToRead = end - start + 1;
		byte[] buffer = new byte[Constants.Size.bufferSize.value];
		int validBytes = buffer.length;

		while ((validBytes = istream.read(buffer)) > 0) {
			try {
				// if at end of input stream
				if (validBytes < 0) {
					exception = new IOException(
							"Attempt to read past end of input.");
				}
				// if all bytes read should be written
				else if (bytesToRead >= validBytes) {
					ostream.write(buffer, 0, validBytes);
					bytesToRead = bytesToRead - validBytes;
				}
				// otherwise only write those requested
				else {
					ostream.write(buffer, 0, (int) bytesToRead);
					bytesToRead = 0;
				}
			} catch (IOException e) {
				exception = e;
				break;
			}
		}
		return exception;
	}

	@Override
	public String deleteFile(String tenantId, String fileId) {
		return getStreamingDAO().deleteFile(tenantId, fileId);
	}

	@Override
	public List<FileInfo> doUpload(String tenantId, MultipartFile[] file)
			throws IOException, InterruptedException {
		List<FileInfo> listFileInfo = new ArrayList<FileInfo>();
		
		for (MultipartFile multipartFile : file) {
			if (multipartFile.getSize() > 0L) {
				FileInfo fileInfo = null;
				Map<String, Object> fieldMap = new HashMap<String, Object>();
				String cType = multipartFile.getContentType();
				if (cType == null)
					cType = "";

				String fileNameReceived = multipartFile.getOriginalFilename() == null
						|| multipartFile.getOriginalFilename() == ""
						|| multipartFile.getOriginalFilename().length() < 1 ? multipartFile
						.getName() == null
						|| multipartFile.getName() == ""
						|| multipartFile.getName().length() < 1 ? "Test"
						: multipartFile.getName() : multipartFile
						.getOriginalFilename();
				// get file extension
				String ext = fileNameReceived.length() > 0
						&& fileNameReceived.contains(".") ? fileNameReceived
						.substring(fileNameReceived.lastIndexOf(".") + 1) : "";

				// get file name
				String name = fileNameReceived.length() > 0
						&& fileNameReceived.contains(".") ? fileNameReceived
						.substring(0, fileNameReceived.lastIndexOf('.'))
						: "Test";

				if (cType.contains("video")) {

					// get temporary file path and name
					String tmpFile = getFilePathNName(multipartFile);

					LOG.info("ffmpegexePath directory is: " + ffmpegexePath);
					LOG.info("qtfaststartPath directory is: " + qtfaststartPath);

					// create thumbnail for any videos
					FLVConverter fLVConverter = new FLVConverter(ffmpegexePath,
							qtfaststartPath,ffprobeexePath);

					fieldMap = fLVConverter.generateThumbnails(tmpFile,
							getFinalOutputFilePathNName(name, "jpg"),
							thumbWidth, thumbHeight, thumbHour, thumbMinute,
							thumbSecond);
					
					// get input file path and name
					String inputFilePath = getFilePathNName(file[0]);
					
					int duration = fLVConverter.getDuration(inputFilePath);
					System.out.println("duration calculated:" + duration);
						fieldMap.put("durationInSecs",String.valueOf(duration));
					
					String converterOutputFile = name + ".mp4";

					// File to save
					String faststartOutputFile = getFinalOutputFilePathNName(
							name, "mp4");

					fieldMap.put("contentType", cType);
					fieldMap.put("extension", ext);
					fieldMap.put("fileName", name + "." + ext);

					// convert in to .mp4 file
					if (ext.compareToIgnoreCase("mp4") != 0
							&& multipartFile.getContentType().lastIndexOf(
									"image") != 0) {
						LOG.info("File conversion started.........");
						fLVConverter.convert(tmpFile, converterOutputFile);

						fLVConverter.faststart(converterOutputFile,
								faststartOutputFile);
						LOG.info("File converted Successfully......");

						File fileToSave = new File(faststartOutputFile);

						fieldMap.put("file", fileToSave);
						fieldMap.put("extension", "mp4");
						fieldMap.put("fileName", name + ".mp4");
						try {
							fileInfo = getStreamingDAO().saveFile(tenantId,
									fieldMap);
						} catch (Exception e) {
							e.printStackTrace();
						}

						// delete file from system
						fileToSave.delete();
						new File(converterOutputFile).delete();
					} else {
						fieldMap.put("file", multipartFile);
						try {
							fileInfo = getStreamingDAO().saveFile(tenantId,
									fieldMap);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					// delete multi part file transferred to tmp location
					new File(tmpFile).delete();
				} else {
					fieldMap.put("contentType", cType);
					fieldMap.put("extension", ext);
					fieldMap.put("file", multipartFile);
					fieldMap.put("fileName", name + "." + ext);
					try {
						fileInfo = getStreamingDAO().saveFile(tenantId,
								fieldMap);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				listFileInfo.add(fileInfo);
			}
		}
		return listFileInfo;
	}

	private String getFilePathNName(MultipartFile multipartFile) {

		// get file name
		String name = multipartFile.getOriginalFilename() == null
				|| multipartFile.getOriginalFilename() == ""
				|| multipartFile.getOriginalFilename().length() < 1 ? multipartFile
				.getName() == null
				|| multipartFile.getName() == ""
				|| multipartFile.getName().length() < 1 ? "Test"
				: multipartFile.getName() : multipartFile.getOriginalFilename();

		if (!multipartFile.isEmpty()) {
			try {
				byte[] bytes = multipartFile.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				LOG.info("Server File Location=" + serverFile.getAbsolutePath());

				return serverFile.getAbsolutePath();
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + name
					+ " because the file was empty.";
		}

	}

	private String getFinalOutputFilePathNName(String name, String ext) {

		// Creating the directory to store file
		String rootPath = System.getProperty("catalina.home");
		File dir = new File(rootPath + File.separator + "tmpFiles"
				+ File.separator + "outputFiles");
		if (!dir.exists())
			dir.mkdirs();
		return dir.getAbsolutePath() + File.separator + name + "." + ext;
	}

	@Override
	public FileInfo doDigitalSignature(String tenantId, MultipartFile[] file)
			throws IOException, InterruptedException {

		FileInfo fileInfo = new FileInfo();

		if (file != null && file.length == 2 && file[0].getSize() > 0L
				&& file[1].getSize() > 0L) {
			String fileNameReceived = file[0].getOriginalFilename() == null
					|| file[0].getOriginalFilename() == ""
					|| file[0].getOriginalFilename().length() < 1 ? file[0]
					.getName() == null
					|| file[0].getName() == ""
					|| file[0].getName().length() < 1 ? "Test" : file[0]
					.getName() : file[0].getOriginalFilename();
			// get file extension
			String ext = fileNameReceived.length() > 0
					&& fileNameReceived.contains(".") ? fileNameReceived
					.substring(fileNameReceived.lastIndexOf(".") + 1) : "";

			// get file name
			String name = fileNameReceived.length() > 0
					&& fileNameReceived.contains(".") ? fileNameReceived
					.substring(0, fileNameReceived.lastIndexOf('.')) : "Test";

			Map<String, Object> fieldMap = new HashMap<String, Object>();
			String cType = file[0].getContentType();
			if (cType == null)
				cType = "image";

			// get input file path and name
			String inputFilePath = getFilePathNName(file[0]);
			// get signature file path and name
			String signFilePath = getFilePathNName(file[1]);

			// File to save
			String outputFileName = name + "." + ext;
			String outputFilePath = getFinalOutputFilePathNName(name, ext);

			FLVConverter fLVConverter = new FLVConverter(ffmpegexePath,
					qtfaststartPath, ffprobeexePath);
			// get duration of the file
			int duration = fLVConverter.getDuration(inputFilePath);
			int signatureBeginTimeInSec = 0;
			int signatureEndTimeInSec = duration;

			// generate digital signature
			fLVConverter.digitalSignature(cType, inputFilePath, outputFilePath,
					signFilePath, signatureBeginTimeInSec,
					signatureEndTimeInSec);

			LOG.info("Digital Signature Added Successfully......");

			File digitalFileToSave = new File(outputFilePath);

			if (cType.contains("video")) {

				// create thumbnail for any videos

				fieldMap = fLVConverter.generateThumbnails(
						digitalFileToSave.getAbsolutePath(),
						getFinalOutputFilePathNName(name, "jpg"), thumbWidth,
						thumbHeight, thumbHour, thumbMinute, thumbSecond);
				fieldMap.put("contentType", cType);
				fieldMap.put("fileName", name + "." + ext);
				// convert in to .mp4 file
				if (ext.compareToIgnoreCase("mp4") != 0) {
					String converterOutputFile = name + ".mp4";
					String faststartOutputFile = getFinalOutputFilePathNName(
							name, "mp4");
					LOG.info("File conversion started.........");
					fLVConverter.convert(digitalFileToSave.getAbsolutePath(),
							converterOutputFile);

					fLVConverter.faststart(converterOutputFile,
							faststartOutputFile);
					LOG.info("File converted Successfully......");

					File fileToSave = new File(faststartOutputFile);

					fieldMap.put("file", fileToSave);
					fieldMap.put("extension", "mp4");
					fieldMap.put("fileName", name + ".mp4");
					try {
						fileInfo = getStreamingDAO().saveFile(tenantId,
								fieldMap);
					} catch (Exception e) {
						e.printStackTrace();
					}

					// delete file from system
					fileToSave.delete();
					new File(converterOutputFile).delete();
				} else {
					fieldMap.put("extension", ext);
					fieldMap.put("file", digitalFileToSave);
					try {
						fileInfo = getStreamingDAO().saveFile(tenantId,
								fieldMap);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				fieldMap.put("contentType", cType);
				fieldMap.put("extension", ext);
				fieldMap.put("file", digitalFileToSave);
				fieldMap.put("fileName", name + "." + ext);
				try {
					fileInfo = getStreamingDAO().saveFile(tenantId, fieldMap);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// delete multi part file transferred to tmp location
			digitalFileToSave.delete();
			new File(outputFileName).delete();
		}
		return fileInfo;
	}

	@Override
	public void doFileDownload(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String tenantId = request.getHeader("tenantId");
		FileInfo fileInfo = new FileInfo(request.getParameter("fileId"));

		// get file input stream
		InputStream inputStream = getStreamingDAO().getInputStreamById(
				tenantId, fileInfo);

		// gets MIME type of the file
		String mimeType = fileInfo.getContentType();
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}

		// set content attributes for the response
		response.setContentType(mimeType);
		response.setContentLength(fileInfo.getLength().intValue());

		// set headers for the response
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				fileInfo.getFileName());
		response.setHeader(headerKey, headerValue);

		// get output stream of the response
		ServletOutputStream oStream = response.getOutputStream();

		byte[] buffer = new byte[Constants.Size.bufferSize.value];
		int bytesRead = -1;

		// write bytes read from the input stream into the output stream
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			oStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		oStream.close();
	}

	@Override
	public FileInfo findFileById(String tenantId, String id) {
		return getStreamingDAO().findFileById(tenantId, id);
	}
	
	
	public List<FileInfo> doUpload(String tenantId, MultipartFile multipartFile)
			throws IOException, InterruptedException {
		List<FileInfo> listFileInfo = new ArrayList<FileInfo>();
		
//		for (MultipartFile multipartFile : file) {
//			if (multipartFile.getSize() > 0L) {
				FileInfo fileInfo = null;
				Map<String, Object> fieldMap = new HashMap<String, Object>();
				String cType = multipartFile.getContentType();
				if (cType == null)
					cType = "";

				String fileNameReceived = multipartFile.getOriginalFilename() == null
						|| multipartFile.getOriginalFilename() == ""
						|| multipartFile.getOriginalFilename().length() < 1 ? multipartFile
						.getName() == null
						|| multipartFile.getName() == ""
						|| multipartFile.getName().length() < 1 ? "Test"
						: multipartFile.getName() : multipartFile
						.getOriginalFilename();
				// get file extension
				String ext = fileNameReceived.length() > 0
						&& fileNameReceived.contains(".") ? fileNameReceived
						.substring(fileNameReceived.lastIndexOf(".") + 1) : "";

				// get file name
				String name = fileNameReceived.length() > 0
						&& fileNameReceived.contains(".") ? fileNameReceived
						.substring(0, fileNameReceived.lastIndexOf('.'))
						: "Test";

				if (cType.contains("video")) {

					// get temporary file path and name
					String tmpFile = getFilePathNName(multipartFile);

					LOG.info("ffmpegexePath directory is: " + ffmpegexePath);
					LOG.info("qtfaststartPath directory is: " + qtfaststartPath);

					// create thumbnail for any videos
					FLVConverter fLVConverter = new FLVConverter(ffmpegexePath,
							qtfaststartPath);

					fieldMap = fLVConverter.generateThumbnails(tmpFile,
							getFinalOutputFilePathNName(name, "jpg"),
							thumbWidth, thumbHeight, thumbHour, thumbMinute,
							thumbSecond);

					String converterOutputFile = name + ".mp4";

					// File to save
					String faststartOutputFile = getFinalOutputFilePathNName(
							name, "mp4");

					fieldMap.put("contentType", cType);
					fieldMap.put("extension", ext);
					fieldMap.put("fileName", name + "." + ext);

					// convert in to .mp4 file
					if (ext.compareToIgnoreCase("mp4") != 0
							&& multipartFile.getContentType().lastIndexOf(
									"image") != 0) {
						LOG.info("File conversion started.........");
						fLVConverter.convert(tmpFile, converterOutputFile);

						fLVConverter.faststart(converterOutputFile,
								faststartOutputFile);
						LOG.info("File converted Successfully......");

						File fileToSave = new File(faststartOutputFile);

						fieldMap.put("file", fileToSave);
						fieldMap.put("extension", "mp4");
						fieldMap.put("fileName", name + ".mp4");
						try {
							fileInfo = getStreamingDAO().saveFile(tenantId,
									fieldMap);
						} catch (Exception e) {
							e.printStackTrace();
						}

						// delete file from system
						fileToSave.delete();
						new File(converterOutputFile).delete();
					} else {
						fieldMap.put("file", multipartFile);
						try {
							fileInfo = getStreamingDAO().saveFile(tenantId,
									fieldMap);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					// delete multi part file transferred to tmp location
					new File(tmpFile).delete();
				} else {
					fieldMap.put("contentType", cType);
					fieldMap.put("extension", ext);
					fieldMap.put("file", multipartFile);
					fieldMap.put("fileName", name + "." + ext);
					try {
						fileInfo = getStreamingDAO().saveFile(tenantId,
								fieldMap);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				listFileInfo.add(fileInfo);
//			}
//		}
		return listFileInfo;
	}
}
