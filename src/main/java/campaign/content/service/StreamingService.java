package campaign.content.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import campaign.content.domain.FileInfo;

public interface StreamingService {

	public List<FileInfo> fileList(String tenantId);

	public String deleteFile(String tenantId, String fileId);

	public List<FileInfo> doUpload(String tenantId, MultipartFile[] file) throws IOException,
			InterruptedException;
	
	public List<FileInfo> doUpload(String tenantId, MultipartFile file) throws IOException,
	InterruptedException;

	public void doStreaming(HttpServletRequest request,
			HttpServletResponse response) throws Exception;

	public FileInfo doDigitalSignature(String tenantId, MultipartFile[] file) throws IOException, InterruptedException;

	public void doFileDownload(HttpServletRequest request,
			HttpServletResponse response) throws IOException;

	public FileInfo findFileById(String tenantId, String id);
}
