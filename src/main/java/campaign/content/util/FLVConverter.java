package campaign.content.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FLVConverter {
	private String ffmpegApp;
	private String faststart;
	private String ffprobeApp;

	public FLVConverter(String ffmpegApp, String faststart) {
		this.ffmpegApp = ffmpegApp;
		this.faststart = faststart;
	}

	public FLVConverter(String ffmpegApp, String faststart, String ffprobeApp) {
		this.ffmpegApp = ffmpegApp;
		this.faststart = faststart;
		this.ffprobeApp = ffprobeApp;
	}

	public void convert(String filenameIn, String filenameOut, int width,
			int height) throws IOException, InterruptedException {

		convert(filenameIn, filenameOut);
	}

	public int convert(String filenameIn, String filenameOut)
			throws IOException, InterruptedException {
		ProcessBuilder processBuilder;
		// i:input files;-y:overide without asking;-b:video bitrate of the
		// output file;-r :frame rate;-ar:Set the audio sampling frequency
		processBuilder = new ProcessBuilder(ffmpegApp, "-y", "-i", filenameIn,
				"-vcodec", "libx264","-crf","25","-s", "1280x720", "-acodec", "libmp3lame", "-ar", "44100", "-ab", "192k", "-f",
				"mp4", filenameOut);

		Process process = processBuilder.start();
		InputStream stderr = process.getErrorStream();
		InputStreamReader isr = new InputStreamReader(stderr);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null)
			;
		{
		}
		return process.waitFor();
	}

	public int faststart(String filenameIn, String filenameOut)
			throws IOException, InterruptedException {
		ProcessBuilder processBuilder;
		processBuilder = new ProcessBuilder(faststart, filenameIn, filenameOut);
		Process process = processBuilder.start();
		return process.waitFor();
	}

	public Map<String, Object> generateThumbnails(String filename, String dir,
			String width, String height, String hour, String min, String sec)
					throws IOException, InterruptedException {
		Map<String, Object> fieldMap = new HashMap<String, Object>();
		ProcessBuilder processBuilder;

		processBuilder = new ProcessBuilder(ffmpegApp, "-y", "-i", filename,
				"-vframes", "1", "-ss", hour + ":" + min + ":" + sec, "-f",
				"mjpeg", "-s", width + "*" + height, "-an", dir);
		// processBuilder = new ProcessBuilder(ffmpegApp , "-itsoffset" , "-14"
		// ,"-i" ,filename, "-vcodec", "mjpeg", "-vframes", "1", "-an" ,"-f", 
		// "rawvideo", "-s ","320x240", dir);
		Process process = processBuilder.start();
		process.waitFor();

		String sb = consumeStream(process.getErrorStream());
		// consumeStream( process.getInputStream() );
		process.getOutputStream().close();

		Pattern pattern = Pattern.compile("Duration: (.*?),");

		Matcher matcher = pattern.matcher(sb);

		if (matcher.find()) {

			String time = matcher.group(1);
			fieldMap.put("duration", time);
			fieldMap.put("thumb", dir);

		}

		return fieldMap;
	}

	protected String consumeStream(InputStream is) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	public int digitalSignature(String inputFileType, String inputFilePath,
			String outputFilePath, String signatureFilePath,
			int signatureBeginTimeInSec, int signatureEndTimeInSec)
					throws IOException, InterruptedException {
		ProcessBuilder processBuilder;
		try {

			if (inputFileType.contains("image")) {	
				processBuilder = new ProcessBuilder(ffmpegApp, "-i"
						, inputFilePath
						, "-i"
						, signatureFilePath
						, "-filter_complex","\"overlay=main_w-overlay_w-5:main_h-overlay_h-5\""
						, outputFilePath);

				Process process = processBuilder.start();
				InputStream stderr = process.getErrorStream();
				InputStreamReader isr = new InputStreamReader(stderr);
				BufferedReader br = new BufferedReader(isr);
				String line;
				while ((line = br.readLine()) != null)
					;
				{
				}
				return process.waitFor();
			} else if (inputFileType.contains("video")) {

				processBuilder = new ProcessBuilder(ffmpegApp, "-i"
						, inputFilePath
						, "-i"
						, signatureFilePath
						, "-filter_complex","\"overlay=main_w-overlay_w-5:main_h-overlay_h-5:'enable=between(t\\,"
								+ signatureBeginTimeInSec + "\\,"
								+ signatureEndTimeInSec + ")'\" "
								, outputFilePath);

				Process process = processBuilder.start();
				InputStream stderr = process.getErrorStream();
				InputStreamReader isr = new InputStreamReader(stderr);
				BufferedReader br = new BufferedReader(isr);
				String line;
				while ((line = br.readLine()) != null)
					;
				{
				}
				return process.waitFor();
			}else {
				processBuilder = new ProcessBuilder(ffmpegApp, "-i"
						, inputFilePath
						, "-i"
						, signatureFilePath
						, "-filter_complex","\"overlay=main_w-overlay_w-5:main_h-overlay_h-5\""
						, outputFilePath);

				Process process = processBuilder.start();
				InputStream stderr = process.getErrorStream();
				InputStreamReader isr = new InputStreamReader(stderr);
				BufferedReader br = new BufferedReader(isr);
				String line;
				while ((line = br.readLine()) != null)
					;
				{
				}
				return process.waitFor();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getDuration(String filename) throws IOException, InterruptedException{
		int duration = -1;
		try{
			ProcessBuilder processBuilder = new ProcessBuilder(ffprobeApp, "-v", "error", 
					"-show_entries", "format=duration", "-of", "default=noprint_wrappers=1:nokey=1", filename);

			Process process = processBuilder.start();
			process.waitFor();
			String sb =  consumeStream( process.getInputStream() );
			process.getOutputStream().close();

			Double dur = Double.parseDouble(sb);
			duration = dur.intValue() + 1;
		}catch(IOException e){
			e.printStackTrace();
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return duration;
	}

}
