package campaign.content.util;

public class Range {

	public long start;
	public long end;
	public long length;

	boolean validate() {
		if (end >= length) {
			end = length - 1;
		}
		return (start >= 0) && (end >= 0) && (start <= end) && (length > 0);
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
}
