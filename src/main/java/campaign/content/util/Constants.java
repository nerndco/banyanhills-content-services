package campaign.content.util;

public class Constants {
	
	public enum ContentType {
		 VIDEO_MP4("video/mp4"),
		 IMAGETYPE("image/jpeg"),
		 MULTIPART_BYTERANGE("multipart/byteranges");
		 
		public String value;

		public String stringValue() {
			return value;
		}
		
		private ContentType(String value) {
			this.value = value;
		}
	}
	
	public enum MimeType {
		mimeSeparation("gvps-mime-boundary");
		public String value;

		public String stringValue() {
			return value;
		}
		private MimeType(String value) {
			this.value = value;
		}
		
	}
	
	public enum Size{
		responseBufferSize(1024 * 16),
		bufferSize(1024 * 16);
		
		public int value;
		
		public int Value(){
			return value;
		}
		
		private Size(int value){
		this.value=value;
		}
	}
}
