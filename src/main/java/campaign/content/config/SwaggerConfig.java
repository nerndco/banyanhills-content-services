/**
 * 
 */
package campaign.content.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Deepak
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() { 
    	String basePackage = "campaign.content.controller";
    	String pathResource = "/content/api/*";
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage(basePackage))              
          .paths(PathSelectors.ant(pathResource))                          
          .build()
          .apiInfo(apiInfo());                                           
    }


	/**
	 * @return ApiInfo this will be display on the swagger ui page 
	 */
	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo(
			      "Content Services API",
			      "Campaign Management System.",
			      "v1",
			      "Terms of service",
			      new Contact("Deepak","","deepak@infogain.com"),
			      "",
			      "");
			    return apiInfo;
	}
}
