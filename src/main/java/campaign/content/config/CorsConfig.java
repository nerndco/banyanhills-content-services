package campaign.content.config;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
class CorsConfig {

                /*@Bean
                public WebMvcConfigurer corsConfigurer() {

                                return new WebMvcConfigurerAdapter() {
                                                                                @Override
                                                                                public void addCorsMappings(CorsRegistry registry) {
                                                                                                registry.addMapping("/v1/**").allowedMethods("OPTIONS","GET", "POST", "PUT", "DELETE")
                                                                                }
                                                                }
                }*/

                @Bean
                public FilterRegistrationBean corsFilter() {
                UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
                CorsConfiguration config = new CorsConfiguration();
                config.setAllowCredentials(true);
                config.addAllowedOrigin("*");
                config.addAllowedHeader("*");
                config.addAllowedMethod("OPTIONS");
                config.addAllowedMethod("HEAD");
                config.addAllowedMethod("GET");
                config.addAllowedMethod("PUT");
                config.addAllowedMethod("POST");
                config.addAllowedMethod("DELETE");
                config.addAllowedMethod("PATCH");
                source.registerCorsConfiguration("/**", config);
                // return new CorsFilter(source);
                final FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
                bean.setOrder(0);
                return bean;
                }
}
