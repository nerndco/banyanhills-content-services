package campaign.content.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
@Document(collection = "content")
public class Content implements Serializable {

	private static final long serialVersionUID = -112064751282001021L;

	@Id
	private String id;
	private String name;
	private String type;
	private String usage;
	private Integer size;
	private String extension;
	private Long likeCount;
	private Long dislikeCount;
	private Comment comments[];
	private String gridFsId;

	private Integer x;
	private Integer y;
	private Integer w;
	private Integer h;
	private Integer radious;

	private ContentExtension extensions[];
	private String createdByUserId;
	private String createdByUserName;
	private Date createdByOn;
	private String modifiedByUserId;
	private String modifiedByUserName;
	private Date modifiedByOn;
	private String tenantId;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the usage
	 */
	public String getUsage() {
		return usage;
	}

	/**
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(String usage) {
		this.usage = usage;
	}

	/**
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(Integer size) {
		this.size = size;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the likeCount
	 */
	public Long getLikeCount() {
		return likeCount;
	}

	/**
	 * @param likeCount
	 *            the likeCount to set
	 */
	public void setLikeCount(Long likeCount) {
		this.likeCount = likeCount;
	}

	/**
	 * @return the dislikeCount
	 */
	public Long getDislikeCount() {
		return dislikeCount;
	}

	/**
	 * @param dislikeCount
	 *            the dislikeCount to set
	 */
	public void setDislikeCount(Long dislikeCount) {
		this.dislikeCount = dislikeCount;
	}

	/**
	 * @return the comments
	 */
	public Comment[] getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(Comment[] comments) {
		this.comments = comments;
	}

	/**
	 * @return the gridfsId
	 */
	public String getGridFsId() {
		return gridFsId;
	}

	/**
	 * @param gridfsId
	 *            the gridfsId to set
	 */
	public void setGridFsId(String gridfsId) {
		this.gridFsId = gridfsId;
	}

	/**
	 * @return the x
	 */
	public Integer getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(Integer x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(Integer y) {
		this.y = y;
	}

	/**
	 * @return the w
	 */
	public Integer getW() {
		return w;
	}

	/**
	 * @param w
	 *            the w to set
	 */
	public void setW(Integer w) {
		this.w = w;
	}

	/**
	 * @return the h
	 */
	public Integer getH() {
		return h;
	}

	/**
	 * @param h
	 *            the h to set
	 */
	public void setH(Integer h) {
		this.h = h;
	}

	/**
	 * @return the radious
	 */
	public Integer getRadious() {
		return radious;
	}

	/**
	 * @param radious
	 *            the radious to set
	 */
	public void setRadious(Integer radious) {
		this.radious = radious;
	}

	/**
	 * @return the extensions
	 */
	public ContentExtension[] getExtensions() {
		return extensions;
	}

	/**
	 * @param extensions
	 *            the extensions to set
	 */
	public void setExtensions(ContentExtension[] extensions) {
		this.extensions = extensions;
	}

	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}

	/**
	 * @param createdByUserId
	 *            the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}

	/**
	 * @param createdByUserName
	 *            the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	/**
	 * @return the createdByOn
	 */
	public Date getCreatedByOn() {
		return createdByOn;
	}

	/**
	 * @param createdByOn
	 *            the createdByOn to set
	 */
	public void setCreatedByOn(Date createdByOn) {
		this.createdByOn = createdByOn;
	}

	/**
	 * @return the modifiedByUserId
	 */
	public String getModifiedByUserId() {
		return modifiedByUserId;
	}

	/**
	 * @param modifiedByUserId
	 *            the modifiedByUserId to set
	 */
	public void setModifiedByUserId(String modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}

	/**
	 * @return the modifiedByUserName
	 */
	public String getModifiedByUserName() {
		return modifiedByUserName;
	}

	/**
	 * @param modifiedByUserName
	 *            the modifiedByUserName to set
	 */
	public void setModifiedByUserName(String modifiedByUserName) {
		this.modifiedByUserName = modifiedByUserName;
	}

	/**
	 * @return the modifiedByOn
	 */
	public Date getModifiedByOn() {
		return modifiedByOn;
	}

	/**
	 * @param modifiedByOn
	 *            the modifiedByOn to set
	 */
	public void setModifiedByOn(Date modifiedByOn) {
		this.modifiedByOn = modifiedByOn;
	}

	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId
	 *            the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Content() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Content [id=" + id + ", name=" + name + ", type=" + type
				+ ", usage=" + usage + ", size=" + size + ", extension="
				+ extension + ", likeCount=" + likeCount + ", dislikeCount="
				+ dislikeCount + ", comments=" + Arrays.toString(comments)
				+ ", gridFsId=" + gridFsId + ", x=" + x + ", y=" + y + ", w="
				+ w + ", h=" + h + ", radious=" + radious + ", extensions="
				+ Arrays.toString(extensions) + ", createdByUserId="
				+ createdByUserId + ", createdByUserName=" + createdByUserName
				+ ", createdByOn=" + createdByOn + ", modifiedByUserId="
				+ modifiedByUserId + ", modifiedByUserName="
				+ modifiedByUserName + ", modifiedByOn=" + modifiedByOn
				+ ", tenantId=" + tenantId + "]";
	}

	/**
	 * @param name
	 * @param type
	 * @param usage
	 * @param size
	 * @param extension
	 * @param likeCount
	 * @param dislikeCount
	 * @param comments
	 * @param gridFsId
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param radious
	 * @param extensions
	 * @param createdByUserId
	 * @param createdByUserName
	 * @param createdByOn
	 * @param modifiedByUserId
	 * @param modifiedByUserName
	 * @param modifiedByOn
	 * @param tenantId
	 */
	public Content(String name, String type, String usage, Integer size,
			String extension, Long likeCount, Long dislikeCount,
			Comment[] comments, String gridFsId, Integer x, Integer y,
			Integer w, Integer h, Integer radious,
			ContentExtension[] extensions, String createdByUserId,
			String createdByUserName, Date createdByOn,
			String modifiedByUserId, String modifiedByUserName,
			Date modifiedByOn, String tenantId) {
		super();
		this.name = name;
		this.type = type;
		this.usage = usage;
		this.size = size;
		this.extension = extension;
		this.likeCount = likeCount;
		this.dislikeCount = dislikeCount;
		this.comments = comments;
		this.gridFsId = gridFsId;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.radious = radious;
		this.extensions = extensions;
		this.createdByUserId = createdByUserId;
		this.createdByUserName = createdByUserName;
		this.createdByOn = createdByOn;
		this.modifiedByUserId = modifiedByUserId;
		this.modifiedByUserName = modifiedByUserName;
		this.modifiedByOn = modifiedByOn;
		this.tenantId = tenantId;
	}

}
