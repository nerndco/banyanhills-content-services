package campaign.content.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="contentExtension")
public class ContentExtension implements Serializable{

	private static final long serialVersionUID = 8343686746100144796L;
	
	@Id
	private String id;
	private Integer startSec;
	private Integer duration;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the startSec
	 */
	public Integer getStartSec() {
		return startSec;
	}
	/**
	 * @param startSec the startSec to set
	 */
	public void setStartSec(Integer startSec) {
		this.startSec = startSec;
	}
	/**
	 * @return the duration
	 */
	public Integer getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public ContentExtension(){

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContentExtension [id=" + id + ", startSec=" + startSec
				+ ", duration=" + duration + "]";
	}
	
	
}
