package campaign.content.domain;

public class FileInfo {

	private String fileId;
	private String fileName;
	private Long length;
	private String extension;
	private String contentType;
	private String tenantId;
	private String duration;
	private String thumbId;

	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId
	 *            the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the length
	 */
	public Long getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(Long length) {
		this.length = length;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType
	 *            the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId
	 *            the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getThumbId() {
		return thumbId;
	}

	public void setThumbId(String thumbId) {
		this.thumbId = thumbId;
	}

	public FileInfo() {
	}

	/**
	 * @param fileId
	 */
	public FileInfo(String fileId) {
		this.fileId = fileId;
	}

	/**
	 * @param fileId
	 * @param fileName
	 */
	public FileInfo(String fileId, String fileName) {
		this.fileId = fileId;
		this.fileName = fileName;
	}

	/**
	 * @param fileId
	 * @param fileName
	 * @param length
	 */
	public FileInfo(String fileId, String fileName, Long length) {
		this.fileId = fileId;
		this.fileName = fileName;
		this.length = length;
	}

	/**
	 * @param fileId
	 * @param fileName
	 * @param length
	 * @param extension
	 */
	public FileInfo(String fileId, String fileName, Long length,
			String extension) {
		this.fileId = fileId;
		this.fileName = fileName;
		this.length = length;
		this.extension = extension;
	}

	/**
	 * @param fileId
	 * @param fileName
	 * @param length
	 * @param extension
	 * @param contentType
	 */
	public FileInfo(String fileId, String fileName, Long length,
			String extension, String contentType) {
		this.fileId = fileId;
		this.fileName = fileName;
		this.length = length;
		this.extension = extension;
		this.contentType = contentType;
	}

	/**
	 * @param fileId
	 * @param fileName
	 * @param length
	 * @param extension
	 * @param contentType
	 * @param tenantId
	 */
	public FileInfo(String fileId, String fileName, Long length,
			String extension, String contentType,  String tenantId) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.length = length;
		this.extension = extension;
		this.contentType = contentType;
		this.tenantId = tenantId;
	}
	
	
	/**
	 * @param fileId
	 * @param fileName
	 * @param length
	 * @param extension
	 * @param contentType
	 * @param duration
	 * @param thumbId
	 * @param tenantId
	 */
	public FileInfo(String fileId, String fileName, Long length,
			String extension, String contentType, String duration, String thumbId, String tenantId) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.length = length;
		this.extension = extension;
		this.contentType = contentType;
		this.duration = duration;
		this.thumbId = thumbId;
		this.tenantId = tenantId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FileInfo [fileId=" + fileId + ", fileName=" + fileName
				+ ", length=" + length + ", extension=" + extension
				+ ", contentType=" + contentType + ", tenantId=" + tenantId
				+ "]";
	}

}
