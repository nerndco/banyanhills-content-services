package campaign.content.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import campaign.content.domain.Content;
import campaign.content.domain.QContent;

import com.mysema.query.types.path.StringPath;

/**
 * @author Deepak
 *
 */
public interface ContentRepository extends MongoRepository<Content,String>, QueryDslPredicateExecutor<Content>, QuerydslBinderCustomizer<QContent> {

	public List<Content> findContentByTenantIdAndName(String tenantId, String name);

	@Override
	  default public void customize(QuerydslBindings bindings, QContent content) {

	    bindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value)); 
/*	    bindings.excluding(content.id);  
	    bindings.excluding(content.gridFsId); */
	  }

	public Content findContentByTenantIdAndId(String tenantId, String id);

	public List<Content> findContentByTenantId(String tenantId);
}
