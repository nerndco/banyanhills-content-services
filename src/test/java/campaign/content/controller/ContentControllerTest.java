package campaign.content.controller;

import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import campaign.content.ContentAbstractControllerTest;
import campaign.content.dao.ContentDao;
import campaign.content.domain.Content;
import campaign.content.service.StreamingService;

public class ContentControllerTest extends ContentAbstractControllerTest {
	@Autowired
	private ContentDao contentDao;

	@Autowired
	private StreamingService streamingService;

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testCreateContent() throws Exception {
		String uri = "/content/api/createContent";
		Content content = new Content();
		content.setComments(null);
		content.setCreatedByOn(new Date());
		content.setCreatedByUserId("createdByUserId");
		content.setCreatedByUserName("createdByUserName");
		content.setDislikeCount(0L);
		content.setExtension("extension");
		content.setGridFsId("gridfsId");
		content.setH(0);
		content.setLikeCount(0L);
		content.setModifiedByOn(new Date());
		content.setModifiedByUserId("modifiedByUserId");
		content.setModifiedByUserName("modifiedByUserName");
		content.setName("name");
		content.setRadious(0);
		content.setSize(0);
		content.setType("type");
		content.setUsage("usage");
		content.setW(0);
		content.setX(0);
		content.setY(0);

		String inputJson = super.mapToJson(content);
		MvcResult result = mvc.perform(
				MockMvcRequestBuilders.post(uri)
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(inputJson))
				.andReturn();
		String responseContent = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);

		content = super.mapFromJson(responseContent, Content.class);
		contentDao.deleteContentById(content.getTenantId(),content.getId());

	}

	@Test
	public void testFindAllContent() throws Exception {
		String uri = "/content/api/listAllContent";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).accept(
						MediaType.APPLICATION_JSON).param("tenantId", "0")).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		int status = mvcResult.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				content.trim().length() > 0);

	}

	@Test
	public void testFindContentById() throws Exception {
		String uri = "/content/api/listContentById";

		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).accept(
						MediaType.APPLICATION_JSON).param("id","0").param("tenantId", "0")).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		int status = mvcResult.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
	}

	@Test
	public void testFindContentByName() throws Exception {
		String uri = "/content/api/listContentByName";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).accept(
						MediaType.APPLICATION_JSON).param("name","name").param("tenantId", "0")).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		int status = mvcResult.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				content.trim().length() > 0);

	}

	@Test
	public void testUpdateContentById() throws Exception {
		String uri = "/content/api/updateContent";
		Content content = new Content();

		String updatedName = "updatedName";
		content.setName(updatedName);

		String inputJson = super.mapToJson(content);
		MvcResult result = mvc.perform(
				MockMvcRequestBuilders.put(uri)
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(inputJson))
				.andReturn();
		String responseContent = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);

		content = super.mapFromJson(responseContent, Content.class);
		contentDao.deleteContentById(content.getTenantId(),content.getId());
	}

	@Test
	public void testDeleteContentById() throws Exception {
		String uri = "/content/api/deleteContent";

		MvcResult result = mvc.perform(
				MockMvcRequestBuilders.delete(uri).accept(
						MediaType.APPLICATION_JSON).param("id","0").param("tenantId", "0")).andReturn();
		String responseContent = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);

		Content content = contentDao.findContentById(null,"id");
		Assert.assertNull("failure - expected content should be null", content);

	}

	@Test
	public void testSearchByContentCriteria() throws Exception {
		String uri = "/content/api/searchContent/?name=name";

		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).accept(
						MediaType.APPLICATION_JSON)).andReturn();
		String responseContent = mvcResult.getResponse().getContentAsString();
		int status = mvcResult.getResponse().getStatus();
		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);
	}

	@Test
	public void testHandleFileUpload() throws Exception {
		String uri = "/content/api/uploadFiles";
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.jpg", "image/jpeg", "some image".getBytes());
        MockMultipartFile secondFile = new MockMultipartFile("data", "filename.mp4", "video/mp4", "some video".getBytes());

		MvcResult result = mvc.perform(MockMvcRequestBuilders.fileUpload(uri)
                .file(firstFile)
                .file(secondFile)).andReturn();
		String responseContent = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);
	}

	@Test
	public void testHandleDigitalSignature() throws Exception {
		String uri = "/content/api/digitalSignature";
        MockMultipartFile mainFile = new MockMultipartFile("data", "filename1.jpg", "image/jpeg", "some image".getBytes());
        MockMultipartFile signFile = new MockMultipartFile("data", "filename2.jpg", "image/jpeg", "some image".getBytes());

		MvcResult result = mvc.perform(MockMvcRequestBuilders.fileUpload(uri)
                .file(mainFile)
                .file(signFile)).andReturn();
		String responseContent = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);
	}

	@Test
	public void testFindAllFiles() throws Exception {
		String uri = "/content/api/listAllFiles";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).accept(
						MediaType.APPLICATION_JSON)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		int status = mvcResult.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				content.trim().length() > 0);

	}

	@Test
	public void testFindFileById() throws Exception {
		String uri = "/content/api/listFileById/?fileId=id";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.get(uri).accept(
						MediaType.APPLICATION_JSON)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		int status = mvcResult.getResponse().getStatus();

		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				content.trim().length() > 0);

	}

	@Test
	public void testStream() throws Exception {
/*		String uri = "/content/api/streamVideo";
		MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.request(HttpMethod.GET, uri).param("fileId", "id")).andReturn().getResponse();
		int status = response.getStatus();
		fail(String.valueOf(response.getContentAsString().length()));
		Assert.assertNotNull("failure - response is null", response);
		Assert.assertEquals("Failure - http status 200 not found", 200, status);
*/
	}

	@Test
	public void testDeleteFileById() throws Exception {
		String uri = "/content/api/deleteFile/?fileId=id";

		MvcResult result = mvc.perform(
				MockMvcRequestBuilders.delete(uri).accept(
						MediaType.APPLICATION_JSON)).andReturn();
		String responseContent = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		Assert.assertEquals("Failure - http status 200 not found", 200, status);
		Assert.assertTrue(
				"Failure - expected HTTP response body to have a value",
				responseContent.trim().length() > 0);

	}

	@Test
	public void testDoDownload() throws Exception {
/*		String uri = "/content/api/downloadFile";
		MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.request(HttpMethod.GET, uri).param("fileId", "id")).andReturn().getResponse();
		int status = response.getStatus();
		Assert.assertNotNull("failure - response is null", response);
		Assert.assertEquals("Failure - http status 200 not found", 200, status);
*/
	}

}
