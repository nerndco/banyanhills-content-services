package campaign.content;


import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ContentApplication.class)
public abstract class ContentAbstractTest {

	protected Logger LOG = LoggerFactory.getLogger(this.getClass());

}
